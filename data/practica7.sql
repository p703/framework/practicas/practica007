﻿DROP DATABASE IF EXISTS practica7;
CREATE DATABASE IF NOT EXISTS practica7;
USE practica7;

CREATE TABLE datos(
  codigoAlumno int AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(100),
  telefono varchar(20),
  correo varchar(100),
  PRIMARY KEY(codigoAlumno)
  );

