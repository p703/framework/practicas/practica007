<?php
use yii\widgets\ListView;


echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'=>'_alumnos',
    "options"=>[
        "class"=>"row"
    ],
    "itemOptions"=>[
        "class"=>"col-lg-3",
        "style"=>"background-color:#ccc",
    ],
    "summaryOptions"=>[
        "class"=>"col-lg-12",
    ]
]);
