<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns'=>[
        'codigoAlumno',
        'nombre',
        'apellidos',
        'correo',
        'telefono',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' =>[
                'delete' => function ($url,$model,$key) {
                    return Html::a('Eliminar',
                            [
                                "site/eliminar",
                                "codigo"=>$model->codigoAlumno
                            ]);
                },

            ]
        ],
    ]
]);
