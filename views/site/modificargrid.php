<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns'=>[
        'codigoAlumno',
        'nombre',
        'apellidos',
        'correo',
        'telefono',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
        ],
    ]
]);