<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Datos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Datos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoAlumno',
            'nombre',
            'apellidos',
            'telefono',
            'correo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
